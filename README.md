# CarCar

Team:

- Anthony Woo - Service microservice
- Carmen Lemoine - Sales microservice

## Getting Started

**Make sure you have Docker, Git, and Node.js 18.2 or above**

1. Fork the repository at www.gitlab.com/anthony-t-woo/project-beta.

2. Clone the repository to your local computer with the command: git clone <<respository.url>>

3. Change working directory to the cloned project: cd <<project-directory>>

4. Build the images and the containers using Docker:

```
	'docker volume create beta-data'
   	'docker-compose build'
   	'docker-compose up'
```

   Ensure all containers are running in Docker.

!['View of all Docker containers running'](/assets/DockerView.png)

5. (optional) Load starter data **_these steps must be performed in this order_**

    | Terminal Command                                  | Action                                                                |
    | ------------------------------------------------- | --------------------------------------------------------------------- |
    | cmd+t(mac) or ctrl+t(windows)                     | start new terminal tab                                                |
    | docker exec -it project-beta-inventory-api-1 bash | interact in inventory container terminal                              |
    | python manage.py loaddata data.json               | load inventory data to inventory database                             |
    | exit                                              | exit docker container interaction - should not stop running container |
    | docker exec -it project-beta-service-api-1 bash   | interact in service container terminal                                |
    | python manage.py loaddata data.json               | load service data to service database                                 |
    | exit                                              | exit docker container interaction - should not stop running container |
    | docker exec -it project-beta-sales-api-1 bash     | interact in sales container terminal                                  |
    | python manage.py loaddata data.json               | load sales data to sales database                                     |

6. Go to http://localhost:3000/ to see the application UI.

!['Image of Application UI upon start'](/assets/ApplicationUIImage.png)

## Design

CarCar is composed of three microservices: Inventory, Service, and Sales. These microservices work together to enable a car dealership to manage its day-to-day operations all in one place.

!['Diagram of microservices'](/assets/ProjectBetaDesignImage.png)

## Inventory microservice

This microservice allows the dealership to track their inventory of vehicles and is based on three interdependent
models: Manufacturer, Vehicle, and Automobile.

### Manufacturer APIs

You can create, update, delete, and list a manufacturer, as well as list all manufacturers.

| Action                         | Method | URL                                               |
| ------------------------------ | ------ | ------------------------------------------------- |
| List manufacturers             | GET    | http://localhost:8100/api/manufacturers/          |
| Create an manufacturer         | POST   | http://localhost:8100/api/manufacturers/          |
| Get a specific manufacturer    | GET    | http://localhost:8100/api/manufacturers/<int:pk>/ |
| Update a specific manufacturer | PUT    | http://localhost:8100/api/manufacturers/<int:pk>/ |
| Delete a specific manufacturer | DELETE | http://localhost:8100/api/manufacturers/<int:pk>/ |

#### List manufacturers

To list all manufacturers, you do not need to send any data when accessing the endpoint URL.

The application returns the following JSON object that contains the name, ID, and the relative path for each manufacturer:

```
{
	"manufacturers": [
		{
			"href": "/api/manufacturers/1/",
			"id": 1,
			"name": "Honda"
		},
		{
			"href": "/api/manufacturers/2/",
			"id": 2,
			"name": "Toyota"
		},
		{
			"href": "/api/manufacturers/3/",
			"id": 3,
			"name": "Subaru"
		}
	]
}
```

#### Create manufacturers

To create a manufacturer, send an object (example provided below) in a JSON body to the endpoint listed above. All you need to register a manufacturer is the "name" property.

If you receive an error, check your spelling and make sure you have provided the correct details.

```
{
  "name": "Toyota"
}
```

A successful request will return a json response containing the newly created manufacturer object with the following properties: href, id, and name. The href and database id are created automatically.

```
{
	"href": "/api/manufacturers/2/",
	"id": 2,
	"name": "Toyota"
}
```

#### Get a specific manufacturer

To view the details of a specific manufacturer, you will need to provide the manufacturer's id (or href) at the end of the endpoint URL:

For example, "http://localhost:8100/api/manufacturers/2/

You do not need to send any data.

A successful request will return a json response that contains the href, id, and name properties for that manufacturer:

```
{
	"href": "/api/manufacturers/2/",
	"id": 2,
	"name": "Toyota"
}

```

#### Update a specific manufacturer

To update the name property of a specific manufacturer, send a PUT request with a JSON body that provides the updated name to the specified href for that manufacturer.

For example, send the following to http://localhost:8100/api/manufacturers/2/ :

```
{
  "name": "Chrysler"
}
```

A successful request will return a json response that contains the href, id, and updated name for that manufacturer:

```
{
	"href": "/api/manufacturers/2/",
	"id": 2,
	"name": "Chrysler"
}
```

#### Delete a specific manufacturer

To delete a specific manufacturer, send a DELETE request to the endpoint URL with that manufacturer's database id:

For example, http://localhost:8100/api/manufacturers/2/

A successful deletion will return a json response that contains the id and name:

```
{
	"id": null,
	"name": "Subaru"
}
```

### Vehicle APIs

You can create, update, delete, and list a vehicle's manufacturer and model, as well as list all vehicles.

| Action                    | Method | URL                                        |
| ------------------------- | ------ | ------------------------------------------ |
| List vehicles             | GET    | http://localhost:8100/api/models/          |
| Create an vehicle         | POST   | http://localhost:8100/api/models/          |
| Get a specific vehicle    | GET    | http://localhost:8100/api/models/<int:pk>/ |
| Update a specific vehicle | PUT    | http://localhost:8100/api/models/<int:pk>/ |
| Delete a specific vehicle | DELETE | http://localhost:8100/api/models/<int:pk>/ |

#### List vehicle models

To list all vehicle models, you do not need to send any data when accessing the endpoint URL.

The application returns the following JSON object that contains the href, id, name, a URL to a picture of that model, and the details of the manufacturer for each vehicle model:

```
{
	"models": [
		{
			"href": "/api/models/1/",
			"id": 1,
			"name": "CR-V",
			"picture_url": "pictureurl.com",
			"manufacturer": {
				"href": "/api/manufacturers/1/",
				"id": 1,
				"name": "Honda"
			}
		},
		{
			"href": "/api/models/2/",
			"id": 2,
			"name": "Tacoma",
			"picture_url": "pictureurl.com",
			"manufacturer": {
				"href": "/api/manufacturers/2/",
				"id": 2,
				"name": "Toyota"
			}
		},
    ]
}
```

#### Create vehicle models

To create a vehicle model, send an object (example provided below) in a JSON body to the endpoint listed above. Provide the following properties: "name", "picture_url", and "manufacturer_id".

If you receive an error, check your spelling and make sure you have provided the correct properties.

```
{
  "name": "Airstream",
  "picture_url": "pictureurl.com",
  "manufacturer_id": 3
}
```

A successful request will return a json response containing the newly created vehicle model object with the following properties: href, id, name, picture URL, and the manufacturer's details. The href and database id are created automatically.

```
{
	"href": "/api/models/4/",
	"id": 4,
	"name": "Airstream",
	"picture_url": "pictureurl.com",
	"manufacturer": {
		"href": "/api/manufacturers/3/",
		"id": 3,
		"name": "Chrysler"
	}
}
```

#### Get a specific vehicle model

To view the details of a specific vehicle model, you will need to provide the vehicle model's id (or href) at the end of the endpoint URL:

For example, "http://localhost:8100/api/models/4/

You do not need to send any data.

A successful request will return a json response that contains the following properties: href, id, name, picture URL, and manufacturer's details for that model.

```
{
	"href": "/api/models/4/",
	"id": 4,
	"name": "Airstream",
	"picture_url": "pictureurl.com",
	"manufacturer": {
		"href": "/api/manufacturers/3/",
		"id": 3,
		"name": "Chrysler"
	}
}

```

#### Update a specific vehicle model

To update the name and/or picture URL properties of a specific manufacturer, send a PUT request with a JSON body that provides the updated name and/or picture URL to the specified href for that vehicle model.

For example, send the following to http://localhost:8100/api/models/4/ :

```
{
  "name": "Cordoba",
  "picture_url": "pictureurl.com"
}
```

A successful request will return a json response that contains the href, id, name, picture URL, and manufacturer's details for that vehicle model:

```
{
	"href": "/api/models/4/",
	"id": 4,
	"name": "Cordoba",
	"picture_url": "pictureurl.com",
	"manufacturer": {
		"href": "/api/manufacturers/3/",
		"id": 3,
		"name": "Chrysler"
	}
}
```

#### Delete a specific vehicle model

To delete a specific vehicle model, send a DELETE request to the endpoint URL with that vehicle model's database id:

For example, http://localhost:8100/api/models/1/

A successful deletion will return a json response that contains the following:

```
{
	"id": null,
	"name": "Sebring",
	"picture_url": "pictureurl.com",
	"manufacturer": {
		"href": "/api/manufacturers/3/",
		"id": 3,
		"name": "Chrysler"
	}
}
```

### Automobile APIs

You can create, update, delete, and list a vehicle's manufacturer and model, as well as list all vehicles.

| Action                       | Method | URL                                              |
| ---------------------------- | ------ | ------------------------------------------------ |
| List automobiles             | GET    | http://localhost:8100/api/automobiles/           |
| Create an automobile         | POST   | http://localhost:8100/api/automobiles/           |
| Get a specific automobile    | GET    | http://localhost:8100/api/automobiles/<str:vin>/ |
| Update a specific automobile | PUT    | http://localhost:8100/api/automobiles/<str:vin>/ |
| Delete a specific automobile | DELETE | http://localhost:8100/api/automobiles/<str:vin>/ |

#### List automobiles

To list all automobiles, you do not need to send any data when accessing the endpoint URL.

The application returns the following JSON object that contains the following properties for each automobile:
-href
-id
-color
-year
-vin
-model details
-manufacturer details
-sold (a Boolean value)

```
{
	"autos": [
		{
			"href": "/api/automobiles/98FDSH78HY657TH00/",
			"id": 4,
			"color": "blue",
			"year": 2022,
			"vin": "98FDSH78HY657TH00",
			"model": {
				"href": "/api/models/2/",
				"id": 2,
				"name": "Tacoma",
				"picture_url": "pictureurl.com",
				"manufacturer": {
					"href": "/api/manufacturers/2/",
					"id": 2,
					"name": "Toyota"
				}
			},
			"sold": true
		},
    ]
}
```

#### Create automobiles

To create an automobile, send an object (example provided below) in a JSON body to the endpoint listed above. Provide the following properties: "color", "year", "vin", and "model_id". ==Note: the VIN must be unique.==

If you receive an error, check your spelling and make sure you have provided the correct properties.

```
{
  "color": "blue",
  "year": 2022,
  "vin": "98FDSH78HY657TH00",
  "model_id": 2
}
```

A successful request will return a json response containing the newly created automobile object with the following properties: -href
-id
-color
-year
-vin
-model details
-manufacturer details
-sold (a Boolean value)

The href and database id are created automatically.

```
{
	"href": "/api/automobiles/98FDSH78HY657TH00/",
	"id": 4,
	"color": "blue",
	"year": 2022,
	"vin": "98FDSH78HY657TH00",
	"model": {
		"href": "/api/models/2/",
		"id": 2,
		"name": "Tacoma",
		"picture_url": "pictureurl.com",
		"manufacturer": {
			"href": "/api/manufacturers/2/",
			"id": 2,
			"name": "Toyota"
		}
	},
	"sold": false
}
```

#### Get a specific automobile

To view the details of a specific automobile, you will need to provide the automobiles's vin (or href) at the end of the endpoint URL:

For example, "http://localhost:8100/api/automobiles/98FDSH78HY657TH00/

You do not need to send any data.

A successful request will return a json response that contains the following properties:
-href
-id
-color
-year
-vin
-model details
-manufacturer details
-sold (a Boolean value)

```
{
	"href": "/api/automobiles/98FDSH78HY657TH00/",
	"id": 4,
	"color": "blue",
	"year": 2022,
	"vin": "98FDSH78HY657TH00",
	"model": {
		"href": "/api/models/2/",
		"id": 2,
		"name": "Tacoma",
		"picture_url": "pictureurl.com",
		"manufacturer": {
			"href": "/api/manufacturers/2/",
			"id": 2,
			"name": "Toyota"
		}
	},
	"sold": false
}
```

#### Update a specific automobile

To update the color, year, or sold properties of a specific automobile, send a PUT request with a JSON body that provides the updated properties to the specified href for that automobile.

For example, send the following to http://localhost:8100/api/automobiles/98FDSH78HY657TH00/ :

```
{
  "color": "Ocean Mist",
  "year": 1997,
  "sold": true
}
```

A successful request will return a json response that contains the href, id, color, year, vin, sold, model details and manufacturer's details for that vehicle model:

```
{
	"href": "/api/automobiles/98FDSH78HY657TH00/",
	"id": 4,
	"color": "Ocean Mist",
	"year": 1997,
	"vin": "98FDSH78HY657TH00",
	"model": {
		"href": "/api/models/2/",
		"id": 2,
		"name": "Tacoma",
		"picture_url": "pictureurl.com",
		"manufacturer": {
			"href": "/api/manufacturers/2/",
			"id": 2,
			"name": "Toyota"
		}
	},
	"sold": true
}
```

#### Delete a specific automobile

To delete a specific automobile, send a DELETE request to the endpoint URL with that automobile's VIN:

For example, http://localhost:8100/api/automobiles/98FDSH78HY657TH00/

A successful deletion will return a json response that contains the following:

```
{
	"href": "/api/automobiles/98FDSH78HY657TH00/",
	"id": null,
	"color": "Ocean Mist",
	"year": 1997,
	"vin": "98FDSH78HY657TH00",
	"model": {
		"href": "/api/models/2/",
		"id": 2,
		"name": "Tacoma",
		"picture_url": "pictureurl.com",
		"manufacturer": {
			"href": "/api/manufacturers/2/",
			"id": 2,
			"name": "Toyota"
		}
	},
	"sold": true
}
```

---

## Service microservice

The "service" microservice is a way for the service concierge to keep track of service appointments, past and upcoming. The service is capable of listing technicians, creating and deleting technicians, listing appointments, creating appointments, marking appointments as cancelled or finished, and viewing a specific vehicle's service appointment history. The application can be broken down into three essential models: Technician, Appointment, and AutomobileVO(populated from polled data from inventory microservice). Creating an appointment depends on the existence of technicians and therefore instances technicians must be created prior to creating an appointment.

### Technician APIs

| Action              | Method | URL                                            |
| ------------------- | ------ | ---------------------------------------------- |
| List technicians    | GET    | http://localhost:8080/api/technicians/         |
| Create a technician | POST   | http://localhost:8080/api/technicians/         |
| Delete a technician | DELETE | http://localhost:8080/api/technicians/<int:pk> |

#### List technicians API

This endpoint does not require a request body when method is "GET" to show a list of all technicians. Making a GET request to this endpoint will return a list of technicians with each technician object containing the following properties: first_name, last_name, employee_id, id. The employee_id and id are unique properties.

##### Example of successful list technician response

```
{
"technicians": [
{
"first_name": "Tony",
"last_name": "Spaghetti",
"employee_id": "tspaghetti",
"id": 1
},
{
"first_name": "Josh",
"last_name": "Elder",
"employee_id": "jelder",
"id": 2
},
{
"first_name": "Liz",
"last_name": "Gonzalez",
"employee_id": "lgonzalez",
"id": 3
},
{
"first_name": "Anthony",
"last_name": "Woo",
"employee_id": "awoo",
"id": 4
},
{
"first_name": "Carmen",
"last_name": "Lemoine",
"employee_id": "clemoine",
"id": 5
}
]
}
```

#### Create technician API

To make a POST request to this endpoint, a body containing the following properties are required: first_name, last_name, and employee_id(must be unique). A successful request will return a json response containing the newly created technician object with the following properties: first_name, last_name, employee_id, and id.

##### Example of create technician request body

```
{
"first_name": "Cary",
"last_name": "Ello",
"employee_id": "Cello"
}
```

##### Example of successful create technician request response

```
{
"first_name": "Cary",
"last_name": "Ello",
"employee_id": "Cello",
"id": 6
}
```

#### Delete technician API

Make a DELETE request to this api to delete a technician from technician data table. A "DELETE" request to this endpoint with a valid id in place of <int:pk> in the url does not require a request body. For example, to delete the created technician in the example above, make a delete request to the url http://localhost:8080/api/technicians/6/ A successful DELETE request will return a json response containing an object with a property, "Technician Deleted" and a boolean value.

##### Example of successful delete technician request response

```
{
"Technician Deleted": true
}
```

### Appointment APIs

| Action                     | Method | URL                                                   |
| -------------------------- | ------ | ----------------------------------------------------- |
| List service appointments  | GET    | http://localhost:8080/api/serviceappointment/         |
| Create service appointment | POST   | http://localhost:8080/api/serviceappointment/         |
| Delete service appointment | DELETE | http://localhost:8080/api/serviceappointment/<int:id> |

#### List service appointments API

Make a GET request to this endpoint to get a list of service appointments. A successful request will return a json response containing a list of all appointments regardless of status and the data is filtered on the front end. GET request to this endpoint does not need to include a request body.

#### Example of successful list service appointments request

```
{
"appointments": [
{
"date_time": "2024-10-25T14:30:59+00:00",
"reason": "Flat tire.",
"vin": "1C3CC5FB2AN120184",
"customer": "Kobe",
"id": 3,
"status": "CANCELLED",
"technician": "megmar"
},
{
"date_time": "2006-10-25T14:30:59+00:00",
"reason": "They want their brakes checked.",
"vin": "1C3CC5FB2AN120174",
"customer": "Cameron",
"id": 2,
"status": "CANCELLED",
"technician": "awoo"
},
{
"date_time": "2024-10-25T14:30:59+00:00",
"reason": "Flat tire.",
"vin": "1C3CC5FB2AN120184",
"customer": "Kool Kieth",
"id": 4,
"status": "FINISHED",
"technician": "megmar"
}
]
}
```

#### Create service appointment API

To make a POST request to this endpoint, a body containing the following properties are required: vin, customer, date_time, technician, technician(id), and reason. A successful request will return a json response containing the newly created appointment object with the following properties: vin, customer, date_time, technician, technician(name), status and id.

##### Example of POST request to create service appointment API

```
{
"vin": "1C3CC5FB2AN120184",
"customer": "Kool Kieth",
"date_time": "2024-10-25 14:30:59",
"technician": 2,
"reason": "Flat tire."
}
```

##### Example response of successful create service appointment request

```
{
"date_time": "2024-10-25T14:30:59+00:00",
"reason": "Flat tire.",
"vin": "1C3CC5FB2AN120184",
"customer": "Kool Kieth",
"id": 3,
"status": "CANCELLED",
"technician": "megmar"
}
```

#### Delete service appointment API

Make a DELETE request to this api to delete a appointment from appointment data table. A "DELETE" request to this endpoint with a valid id in place of <int:pk> in the url does not require a request body. For example, to delete the created appointment in the example above, make a delete request to the url http://localhost:8080/api/appointments/6/ A successful DELETE request will return a json response containing an object with a property, "Appointment Deleted" and a boolean value.

##### Example response of successful delete service appointment request

```
{
"Appointment Deleted": true
}
```

---

---

## Sales microservice

The Sales microservice helps the user to register and track salespeople, customers, and sales of automobiles.

The Sales microservice uses four models: Salesperson, Customer, Sale, and AutomobileVO. The Sale model is dependent on the Salesperson, Customer, and AutomobileVO models. Because you need automobile information in order to record a sale, the AutomobileVO receives data about the automobiles for sale from the Automobile model data in the Inventory MS via a poller. The data is stored as value objects.

### Salesperson APIs

The user can register and delete a salesperson, as well as list all salespeople, using the endpoints in the table below.

| Action               | Method | URL                                             |
| -------------------- | ------ | ----------------------------------------------- |
| List salespeople     | GET    | http://localhost:8090/api/salespeople/          |
| Create a salesperson | POST   | http://localhost:8090/api/salespeople/          |
| Delete a salesperson | DELETE | http://localhost:8090/api/salespeople/<int:id>/ |

#### Create Salespeople

To create a salesperson, send an object (example provided below) in a JSON body to the endpoint listed above. If you receive an error, check your spelling and make sure you have provieded the correct properties.

```

{
"first_name": "Laurence",
"last_name": "Fishburne",
"employee_id": "lfishburne"
}

```

The application returns the following JSON object, with a database ID assigned automatically:

```

{
"first_name": "Laurence",
"last_name": "Fishburne",
"employee_id": "lfishburne",
"id": 1
}

```

#### List Salespeople

To list all salespeople, you do not need to send any data when accessing the correct URL.

The application returns the following JSON object, with salespeople listed in alphabetical order by last name:

```

{
"salespeople": [
{
"last_name": "Davis",
"first_name": "Viola",
"employee_id": "vdavis",
"id": 2
},
{
"last_name": "Fishburne",
"first_name": "Laurence",
"employee_id": "lfishburne",
"id": 1
},
{
"last_name": "Thornton",
"first_name": "Billy Bob",
"employee_id": "bbthornton",
"id": 3
},

    ]

}

```

#### Delete Salespeople

To delete a salesperson, you will need the database "id" of the salesperson for the endpoint URL.

For example, "http://localhost:8090/api/salespeople/1/"

The application returns the following after a successful deletion:

```

{
"deleted": true
}

```

---

### Customer APIs

The user can register and delete a customer, as well as list all customers, using the endpoints in the table below.

| Action            | Method | URL                                           |
| ----------------- | ------ | --------------------------------------------- |
| List customers    | GET    | http://localhost:8090/api/customers/          |
| Create a customer | POST   | http://localhost:8090/api/customers/          |
| Delete a customer | DELETE | http://localhost:8090/api/customers/<int:id>/ |

#### Create Customers

To create a customer, send an object (example provided below) in a JSON body to the endpoint listed above. If you receive an error, check your spelling and make sure you have provided the correct details.

```

{
"first_name": "Gael-Garcia",
"last_name": "Bernal",
"address": "245 E. 80th St, Manhattan, NY, 10075",
"phone_number": "212-987-6543"
}

```

The application returns the following JSON object, with a database ID assigned automatically:

```

{
"last_name": "Bernal",
"first_name": "Gael-Garcia",
"address": "245 E. 80th St, Manhattan, NY, 10075",
"phone_number": "212-987-6543",
"id": 1
}

```

#### List Customers

To list all customers, you do not need to send any data when accessing the correct URL.

The application returns the following JSON object:

```

{
"customers": [
{
"last_name": "Bernal",
"first_name": "Gael-Garcia",
"address": "245 E. 80th St, Manhattan, NY, 10075",
"phone_number": "212-987-6543",
"id": 1
},
{
"last_name": "McCartney",
"first_name": "Paul",
"address": "123 Abbey Road, Los Angeles, CA 90210",
"phone_number": "212-232-8537",
"id": 2
}
]
}

```

#### Delete Customers

To delete a customer, you will need the database ID of the salesperson for the endpoint URL.

For example, "http://localhost:8090/api/customers/1/"

The application returns the following after a successful deletion:

```

{
"deleted": true
}

```

---

### Sales APIs

The user can register and delete a sale, as well as list all sales, using the endpoints in the table below.

| Action        | Method | URL                                       |
| ------------- | ------ | ----------------------------------------- |
| List sales    | GET    | http://localhost:8090/api/sales/          |
| Create a sale | POST   | http://localhost:8090/api/sales/          |
| Delete a sale | DELETE | http://localhost:8090/api/sales/<int:id>/ |

#### Register Sales

To create a sales, send an object (example provided below) in a JSON body to the endpoint listed above. You will need the VIN of the automobile being sold, as well as the salesperson's and customer's database ID's. If you receive an error, check your spelling and make sure you have provided the correct details.

```

{
"vin": "98FDSH78HY657TH00",
"salesperson": 3,
"customer": 4,
"price": 14900
}

```

The application returns the following JSON object, with a database ID assigned automatically:

```

{
"id": 14,
"salesperson": {
"last_name": "Thornton",
"first_name": "Billy Bob",
"employee_id": "bbthornton",
"id": 3
},
"automobile": {
"vin": "98FDSH78HY657TH00",
"sold": true,
"id": 5
},
"customer": {
"last_name": "Bernal",
"first_name": "Gael-Garcia",
"address": "245 E. 80th St, Manhattan, NY, 10075",
"phone_number": "212-987-6543",
"id": 1
},
"price": 14900
}

```

#### Delete Sales

To delete a sale, you will need the database ID of the sale for the endpoint URL.

For example, "http://localhost:8090/api/sales/1/"

The application returns the following after a successful deletion:

```

{
"deleted": true
}

```

---

---

### Value Objects

Because you need automobile information in order to record a sale or create an appointment, the AutomobileVO for each respective microservice receives data about the automobiles for sale from the Automobile model data (in the Inventory MS) via the microservice's poller. The data is stored as value objects.

```

```

```

```

```

```

```

```

```

```
