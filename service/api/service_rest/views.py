from django.shortcuts import render
from .models import Technician, Appointment
from django.http import JsonResponse
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
from .encoders import TechnicianListEncoder, TechnicianDetailEncoder, AppointmentListEncoder, AppointmentDetailEncoder
import json
from datetime import * 
# Create your views here.


@require_http_methods(["GET", "POST"])
def api_list_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse({'technicians': technicians},
                            TechnicianListEncoder)
    else:
        content = json.loads(request.body)
        technician = Technician.objects.create(**content)
        return JsonResponse(technician, TechnicianDetailEncoder, False)


@require_http_methods(["DELETE"])
def api_technician(request, pk):
    if request.method == "DELETE":
        count, _ = Technician.objects.filter(id=pk).delete()
        return JsonResponse({"Technician Deleted": count >0})


@require_http_methods(["GET", "POST"])
def api_list_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse({'appointments': appointments},
                            AppointmentListEncoder)
    else:
        content = json.loads(request.body)
        technician = Technician.objects.get(id=content["technician"])
        content["technician"] = technician
        appointment = Appointment.create(**content)
        return JsonResponse(appointment, AppointmentDetailEncoder, False)


@require_http_methods(["DELETE"])
def api_appointment(request, pk):
    if request.method == "DELETE":
        count, _ = Appointment.objects.filter(id=pk).delete()
        return JsonResponse({"Appointment Deleted": count >0})


@require_http_methods(["PUT"])
def api_finish_appointment(request, pk):
    appointment = Appointment.objects.get(id=pk)
    appointment.finish()
    return JsonResponse(appointment, AppointmentDetailEncoder, False)


@require_http_methods(["PUT"])
def api_cancel_appointment(request, pk):
    appointment = Appointment.objects.get(id=pk)
    appointment.cancel()
    return JsonResponse(appointment, AppointmentDetailEncoder, False)
