import React, { useState } from "react";

export default function AutomobileForm({ models, loadAutomobiles }) {
  const [color, setColor] = useState("");
  const [year, setYear] = useState("");
  const [vin, setVin] = useState("");
  const [model, setModel] = useState("");
  const [isSubmitted, setIsSubmitted] = useState(false);

  const handleColorChange = (e) => {
    const value = e.target.value;
    setColor(value);
  };

  const handleYearChange = (e) => {
    const value = e.target.value;
    setYear(value);
  };

  const handleVinChange = (e) => {
    const value = e.target.value;
    setVin(value);
  };

  const handleModelChange = (e) => {
    const value = e.target.value;
    setModel(value);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    const data = {
      color: color,
      year: year,
      vin: vin,
      model_id: model,
    };

    const url = "http://localhost:8100/api/automobiles/";
    const fetchOptions = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(url, fetchOptions);
    if (response.ok) {
      setIsSubmitted(true);
      loadAutomobiles();
      setColor("");
      setYear("");
      setModel("");
      setVin("");
    } else {
      console.error(response);
    }
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add a New Automobile</h1>
          <form onSubmit={handleSubmit}>
            <div className="form-floating mb-3">
              <input
                className="form-control"
                onChange={handleColorChange}
                placeholder="Color"
                required
                name="color"
                value={color}
                type="text"
                id="color"
              />{" "}
              <label htmlFor="color">Color</label>
            </div>
            <div className="form-floating mb-3">
              <input
                className="form-control"
                onChange={handleYearChange}
                placeholder="Year"
                required
                name="year"
                value={year}
                type="text"
                id="year"
              />
              <label htmlFor="year">Year</label>
            </div>
            <div className="form-floating mb-3">
              <input
                className="form-control"
                onChange={handleVinChange}
                placeholder="VIN"
                required
                name="vin"
                value={vin}
                type="text"
                id="vin"
              />
              <label htmlFor="vin">VIN</label>
            </div>
            <div className="form-floating mb-3">
              <select
                value={model}
                onChange={handleModelChange}
                name="model"
                id="model"
                className="form-select"
                required
              >
                <option value="">Models</option>
                {models.map((model) => (
                  <option value={model.id} key={model.id}>
                    {model.manufacturer.name} {model.name}
                  </option>
                ))}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
          {isSubmitted === true && (
            <div className="alert alert-success mt-4" id="success-message">
              <h3>You successfully registered a new automobile.</h3>
            </div>
          )}
        </div>
      </div>
    </div>
  );
}
