import { NavLink } from "react-router-dom";

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">
          Vehix
        </NavLink>
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item dropdown">
              <a
                className="nav-link dropdown-toggle"
                href="/"
                role="button"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                Customers
              </a>
              <ul className="dropdown-menu">
                <li className="dropdown-item">
                  <NavLink className="nav-link text-dark" to="/customers">
                    Customers
                  </NavLink>
                </li>
                <li className="dropdown-item">
                  <NavLink className="nav-link text-dark" to="/customers/new">
                    Add a New Customer
                  </NavLink>
                </li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <a
                className="nav-link dropdown-toggle"
                href="/"
                role="button"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                Inventory
              </a>
              <ul className="dropdown-menu">
                <li className="dropdown-item">
                  <NavLink className="nav-link text-dark" to="/manufacturers">
                    Manufacturers
                  </NavLink>
                </li>
                <li className="dropdown-item">
                  <NavLink className="nav-link text-dark" to="/models">
                    Models
                  </NavLink>
                </li>
                <li className="dropdown-item">
                  <NavLink className="nav-link text-dark" to="/automobiles">
                    Automobiles
                  </NavLink>
                </li>
                <li className="dropdown-item">
                  <NavLink
                    className="nav-link text-dark"
                    to="/manufacturers/new"
                  >
                    Create a Manufacturer
                  </NavLink>
                </li>
                <li className="dropdown-item">
                  <NavLink className="nav-link text-dark" to="/models/new">
                    Create a Model
                  </NavLink>
                </li>
                <li className="dropdown-item">
                  <NavLink className="nav-link text-dark" to="/automobiles/new">
                    Create an Automobile
                  </NavLink>
                </li>
              </ul>
            </li>

            <li className="nav-item dropdown">
              <a
                className="nav-link dropdown-toggle"
                href="/"
                role="button"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                Sales
              </a>
              <ul className="dropdown-menu">
                <li className="dropdown-item">
                  <NavLink className="nav-link text-dark" to="/sales">
                    Sales
                  </NavLink>
                </li>
                <li className="dropdown-item">
                  <NavLink className="nav-link text-dark" to="/sales/new">
                    Register a New Sale
                  </NavLink>
                </li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <a
                className="nav-link dropdown-toggle"
                href="/"
                role="button"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                Salespeople
              </a>
              <ul className="dropdown-menu">
                <li className="dropdown-item">
                  <NavLink className="nav-link text-dark" to="/salespeople">
                    Salespeople
                  </NavLink>
                </li>
                <li className="dropdown-item">
                  <NavLink className="nav-link text-dark" to="/salespeople/new">
                    Add a New Salesperson
                  </NavLink>
                </li>
                <li className="dropdown-item">
                  <NavLink className="nav-link text-dark" to="/sales/history">
                    View Salesperson History
                  </NavLink>
                </li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <a
                className="nav-link dropdown-toggle"
                href="/"
                role="button"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                Service Appointments
              </a>
              <ul className="dropdown-menu">
                <li className="dropdown-item">
                  <NavLink className="nav-link text-dark" to="/appointments">
                    Appointments
                  </NavLink>
                </li>
                <li className="dropdown-item">
                  <NavLink
                    className="nav-link text-dark"
                    to="/appointments/new"
                  >
                    Create an Appointment
                  </NavLink>
                </li>
                <li className="dropdown-item">
                  <NavLink
                    className="nav-link text-dark"
                    to="/appointments/history"
                  >
                    Service History
                  </NavLink>
                </li>
              </ul>
            </li>

            <li className="nav-item dropdown">
              <a
                className="nav-link dropdown-toggle"
                href="/"
                role="button"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                Technicians
              </a>
              <ul className="dropdown-menu">
                <li className="dropdown-item">
                  <NavLink className="nav-link text-dark" to="/technicians">
                    Technicians
                  </NavLink>
                </li>
                <li className="dropdown-item">
                  <NavLink className="nav-link text-dark" to="/technicians/new">
                    Create a Technician
                  </NavLink>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
}

export default Nav;
