export default function ListModels({ models }) {
  return (
    <div>
      <div className="row my-3">
        <h1> Vehicle Models </h1>
      </div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Name</th>
            <th>Manufacturer</th>
            <th>Picture</th>
          </tr>
        </thead>
        <tbody>
          {models.map((model) => (
            <tr key={model.id}>
              <td>{model.name}</td>
              <td>{model.manufacturer.name}</td>
              <td>
                <img
                  style={{ height: 150 }}
                  src={model.picture_url}
                  alt="Car"
                ></img>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}
