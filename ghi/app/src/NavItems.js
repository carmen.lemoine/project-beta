export const navItems = [
  {
    id: 1,
    title: "Sales",
    path: "#",
    cName: "nav-item",
    children: [
      {
        id: 1,
        title: "List of Sales",
        path: "/sales",
        cName: "submenu-item",
      },
      {
        id: 2,
        title: "Add Sale",
        path: "/sales/new",
        cName: "submenu-item",
      },
      {
        id: 3,
        title: "Salespeople",
        path: "/salespeople",
        cName: "submenu-item",
      },
      {
        id: 4,
        title: "Add Salesperson",
        path: "/salespeople/new",
        cName: "submenu-item",
      },
      {
        id: 5,
        title: "Salesperson History",
        path: "/sales/history",
        cName: "submenu-item",
      },
    ],
  },
  {
    id: 2,
    title: "Inventory",
    path: "#",
    cName: "nav-item",
    children: [
      {
        id: 1,
        title: "Manufacturers",
        path: "/manufacturers",
        cName: "submenu-item",
      },
      {
        id: 2,
        title: "Vehicle Models",
        path: "/models",
        cName: "submenu-item",
      },
      {
        id: 3,
        title: "Automobile Inventory",
        path: "/automobiles",
        cName: "submenu-item",
      },
    ],
  },
  {
    id: 3,
    title: "Service",
    path: "#",
    cName: "nav-item",
    children: [
      {
        id: 1,
        title: "Appointments",
        path: "/appointments",
        cName: "submenu-item",
      },
      {
        id: 2,
        title: "Add Appointment",
        path: "/appointments/new",
        cName: "submenu-item",
      },
      {
        id: 3,
        title: "Appointment History",
        path: "/appointments/history",
        cName: "submenu-item",
      },
      {
        id: 4,
        title: "Technicians",
        path: "/technicians",
        cName: "submenu-item",
      },
      {
        id: 5,
        title: "Add Technician",
        path: "/technicians/new",
        cName: "submenu-item",
      },
    ],
  },
  {
    id: 4,
    title: "Customers",
    path: "#",
    cName: "nav-item",
    children: [
      {
        id: 1,
        title: "Customer List",
        path: "/customers",
        cName: "submenu-item",
      },
      {
        id: 2,
        title: "Add Customer",
        path: "/customers/new",
        cName: "submenu-item",
      },
    ],
  },
];
