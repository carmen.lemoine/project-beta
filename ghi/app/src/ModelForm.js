import React, { useState } from "react";

export default function ModelForm({ manufacturers, loadModels }) {
  const [name, setName] = useState("");
  const [picture_url, setPictureUrl] = useState("");
  const [manufacturer, setManufacturer] = useState("");
  const [isSubmitted, setIsSubmitted] = useState(false);

  const handleNameChange = (e) => {
    const value = e.target.value;
    setName(value);
  };
  const handlePictureUrlChange = (e) => {
    const value = e.target.value;
    setPictureUrl(value);
  };
  const handleManufacturerChange = (e) => {
    const value = e.target.value;
    setManufacturer(value);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    const data = {
      name: name,
      picture_url: picture_url,
      manufacturer_id: manufacturer,
    };
    const newModelUrl = "http://localhost:8100/api/models/";
    const fetchOptions = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const newModelResponse = await fetch(newModelUrl, fetchOptions);
    if (newModelResponse.ok) {
      setIsSubmitted(true);
      loadModels();
      setName("");
      setPictureUrl("");
      setManufacturer("");
    }
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add a New Vehicle Model</h1>
          <form onSubmit={handleSubmit}>
            <div className="form-floating mb-3">
              <input
                className="form-control"
                onChange={handleNameChange}
                placeholder="Model name"
                required
                name="name"
                value={name}
                type="text"
                id="name"
              />
              <label htmlFor="name">Model Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                className="form-control"
                onChange={handlePictureUrlChange}
                placeholder="Picture URL"
                required
                name="picture_url"
                value={picture_url}
                type="text"
                id="picture_url"
              />
              <label htmlFor="picture_url">Picture URL</label>
            </div>
            <div className="form-floating mb-3">
              <select
                onChange={handleManufacturerChange}
                name="manufacturer"
                id="manufacturer"
                className="form-select"
                required
                value={manufacturer}
              >
                <option value="">Manufacturers</option>
                {manufacturers.map((manufacturer) => (
                  <option value={manufacturer.id} key={manufacturer.id}>
                    {manufacturer.name}
                  </option>
                ))}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
          {isSubmitted === true && (
            <div className="alert alert-success mt-4" id="success-message">
              <h3>You successfully registered a new vehicle model.</h3>
            </div>
          )}
        </div>
      </div>
    </div>
  );
}
