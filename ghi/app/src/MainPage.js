import Lottie from "lottie-react";
import sportscar from "./sportscar.json";

function MainPage() {
  return (
    <div className="px-4 py-5 my-5 text-center">
      <h1 className="display-5 fw-bold">Vehix</h1>
      <div className="col-lg-6 mx-auto">
        <p className="lead mb-3">
          The premiere solution for automobile dealership management!
        </p>
        <Lottie
          animationData={sportscar}
          style={{
            width: 258,
            margin: "0 auto",
            zIndex: -1,
            overflow: "hidden",
            position: "static",
          }}
          loop={true}
        />
      </div>
    </div>
  );
}

export default MainPage;
