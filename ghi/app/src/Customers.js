import React from "react";

export default function ListCustomers({ customers }) {
  return (
    <div>
      <div className="row my-3">
        <h1> Customers </h1>
      </div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Last Name</th>
            <th>First Name</th>
            <th>Phone Number</th>
            <th>Address</th>
          </tr>
        </thead>
        <tbody>
          {customers.map((customer) => {
            return (
              <tr key={customer.id}>
                <td>{customer.last_name}</td>
                <td>{customer.first_name}</td>
                <td>{customer.phone_number}</td>
                <td>{customer.address}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}
