import React, { useEffect } from "react";
export default function Appointments({
  appointments,
  autos,
  loadAppointments,
}) {
  const vins = autos.filter((auto) => auto.sold).map((auto) => auto["vin"]);
  const activeAppointments = appointments.filter(
    (appointment) => appointment.status === "ACTIVE"
  );
  const handleCancel = async (appointment) => {
    const url = `http://localhost:8080/api/appointments/${appointment.id}/cancel/`;
    const fetchConfig = {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      loadAppointments();
    }
  };
  const handleFinish = async (appointment) => {
    const url = `http://localhost:8080/api/appointments/${appointment.id}/finish/`;
    const fetchConfig = {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      loadAppointments();
    }
  };

  useEffect(() => {
    loadAppointments();
  }, []);

  return (
    <div>
      <h1 className="my-3"> Service Appointments</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>VIN</th>
            <th>Is VIP?</th>
            <th>Customer</th>
            <th>Date</th>
            <th>Time</th>
            <th>Technician</th>
            <th>Reason</th>
          </tr>
        </thead>
        <tbody>
          {activeAppointments.map((appointment) => (
            <tr key={appointment.id}>
              <td>{appointment.vin}</td>
              <td>{vins.includes(appointment.vin) ? "Yes" : "No"}</td>
              <td>{appointment.customer}</td>
              <td>{new Date(appointment.date_time).toLocaleDateString()}</td>
              <td>{new Date(appointment.date_time).toLocaleTimeString()}</td>
              <td>{appointment.technician}</td>
              <td>{appointment.reason}</td>
              <td>
                <button
                  onClick={() => handleCancel(appointment)}
                  type="button"
                  className="btn btn-danger"
                >
                  Cancel
                </button>
                <button
                  onClick={() => handleFinish(appointment)}
                  type="button"
                  className="btn btn-success"
                >
                  Finish
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}
