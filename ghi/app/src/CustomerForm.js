import React, { useState } from "react";

export default function AddCustomer({ loadCustomers }) {
  const [first_name, setFirstName] = useState("");
  const [last_name, setLastName] = useState("");
  const [address, setAddress] = useState("");
  const [phone_number, setPhoneNumber] = useState("");
  const [isSubmitted, setIsSubmitted] = useState(false);

  const handleFirstNameChange = (e) => {
    const value = e.target.value;
    setFirstName(value);
  };

  const handleLastNameChange = (e) => {
    const value = e.target.value;
    setLastName(value);
  };

  const handleAddressChange = (e) => {
    const value = e.target.value;
    setAddress(value);
  };

  const handlePhoneChange = (e) => {
    const value = e.target.value;
    setPhoneNumber(value);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    const data = {
      first_name: first_name,
      last_name: last_name,
      address: address,
      phone_number: phone_number,
    };

    const newCustomerURL = "http://localhost:8090/api/customers/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(newCustomerURL, fetchConfig);
    if (response.ok) {
      setIsSubmitted(true);
      loadCustomers();
      setFirstName("");
      setLastName("");
      setAddress("");
      setPhoneNumber("");
    }
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <form onSubmit={handleSubmit} id="create-customer-form">
            <h1 className="card-title">Add a New Customer</h1>
            <div className="form-floating mb-3">
              <input
                onChange={handleFirstNameChange}
                required
                value={first_name}
                placeholder="First Name"
                type="text"
                id="first-name"
                name="first-name"
                className="form-control"
              />
              <label htmlFor="first-name">First Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleLastNameChange}
                required
                value={last_name}
                placeholder="Last Name"
                type="text"
                id="last-name"
                name="last-name"
                className="form-control"
              />
              <label htmlFor="last-name">Last Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleAddressChange}
                required
                value={address}
                placeholder="Address"
                type="text"
                id="address"
                name="address"
                className="form-control"
              />
              <label htmlFor="address">Address</label>
              <div id="addressHelp" className="form-text">
                e.g. 123 Main St, Anytown, NY 12345
              </div>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handlePhoneChange}
                required
                value={phone_number}
                placeholder="Phone Number"
                type="text"
                id="phone_number"
                name="phone_number"
                className="form-control"
              />
              <label htmlFor="phone_number">Phone Number</label>
              <div id="phoneHelp" className="form-text">
                e.g. 212-999-1234
              </div>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
          {isSubmitted === true && (
            <div className="alert alert-success mt-4" id="success-message">
              <h3>You successfully created a new customer.</h3>
            </div>
          )}
        </div>
      </div>
    </div>
  );
}
