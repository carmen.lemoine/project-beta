import React, { useState } from "react";

export default function SalespersonHistory({salespeople, sales, loadSales}) {
    const [selectedSellerID, setSelectedSellerID] = useState('');
    const [isSelected, setIsSelected] = useState(false);

    const filteredSales = sales.filter(
        (sale) => sale.salesperson.id === selectedSellerID
    );


    const handleSellerChange = (e) => {
        const value = parseInt(e.target.value);
        setSelectedSellerID(value);
        setIsSelected(true);
    }

    return (
        <div className="my-5 container">
          <div className="row">
            <div className="col">
              <div className="card shadow">
                <div className="card-body">
                  <form id="list-salehistory-form">
                    <h1 className="card-title">Salesperson History</h1>
                      <div className="mb-3">
                        <select
                            onChange={handleSellerChange}
                            value={selectedSellerID}
                            name="salesperson"
                            id="salesperson"
                            className="form-select"
                            required>
                        <option value="">Select a salesperson</option>
                            {salespeople.map(person => {
                            return(
                                <option value={person.id} key={person.id}>
                                    { person.first_name } {person.last_name }
                                </option>
                        );
                    })}
                        </select>
                      </div>
                    {isSelected === true && (
                        <table className="table table-striped">
                        <thead>
                            <tr>
                                <th>Salesperson</th>
                                <th>Customer</th>
                                <th>VIN</th>
                                <th>Price</th>
                            </tr>
                        </thead>
                        <tbody>
                            {filteredSales.map(sale => {
                                return (
                                <tr key={sale.id}>
                                    <td>{sale.salesperson.first_name} {sale.salesperson.last_name}</td>
                                    <td>{sale.customer.first_name} {sale.customer.last_name}</td>
                                    <td>{sale.automobile.vin}</td>
                                    <td>${sale.price}</td>
                                </tr>
                            )})
                            }
                            {filteredSales.length === 0 && (
                                <tr>
                                    <td>There are no sales for this salesperson.</td>
                                </tr>
                            )}
                        </tbody>
                    </table>
                    )}
                    </form>
                </div>
                </div>
                </div>
            </div>
        </div>
    )
}
