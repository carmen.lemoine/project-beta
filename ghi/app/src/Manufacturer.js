export default function ListManufacturers(props) {
  return (
    <div>
      <div className="row my-3">
        <h1> Manufacturers </h1>
      </div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Name</th>
          </tr>
        </thead>
        <tbody>
          {props.manufacturers?.map((manufacturer) => {
            return (
              <tr key={manufacturer.name}>
                <td>{manufacturer.name}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}
