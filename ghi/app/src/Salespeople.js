import React from "react";

export default function ListSalespeople({ salespeople }) {
  return (
    <div>
      <div className="row my-3">
        <h1> Salespeople </h1>
      </div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Last Name</th>
            <th>First Name</th>
            <th>Employee ID</th>
          </tr>
        </thead>
        <tbody>
          {salespeople.map((salesperson) => {
            return (
              <tr key={salesperson.id}>
                <td>{salesperson.last_name}</td>
                <td>{salesperson.first_name}</td>
                <td>{salesperson.employee_id}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}
