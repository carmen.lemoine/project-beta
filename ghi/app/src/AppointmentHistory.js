import React, { useState } from "react";
export default function AppointmentHistory({
  appointments,
  autos,
  loadAppointments,
}) {
  const vins = autos.filter((auto) => auto.sold).map((auto) => auto["vin"]);
  const [vin, setVin] = useState("");

  const handleVinChange = (e) => {
    const value = e.target.value;
    setVin(value);
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    loadAppointments(vin);
  };
  const handleReset = (e) => {
    e.preventDefault();
    loadAppointments();
    setVin("");
  };

  return (
    <div>
      <div className="row mb-3">
        <h1> Service Appointment History</h1>
      </div>
      <div className="row mb-3">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4 mb-3">
            <form onSubmit={handleSubmit}>
              <div className=" mb-3">
                <div className="form-floating row mb-3">
                  <input
                    className="form-control"
                    onChange={handleVinChange}
                    placeholder="search by VIN..."
                    required
                    name="vin"
                    value={vin}
                    type="text"
                    id="vin"
                  />
                  <label htmlFor="vin">Search by VIN</label>

                  <div className="col mb-3">
                    <button className="mt-3 btn btn-primary">Search</button>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
        <div className="row mb-3">
          <button onClick={handleReset} className="btn btn-primary">
            Show All Appointments
          </button>
        </div>
      </div>

      <div className="row mb-3">
        <table className="table table-striped">
          <thead>
            <tr>
              <th>VIN</th>
              <th>Is VIP?</th>
              <th>Customer</th>
              <th>Date</th>
              <th>Time</th>
              <th>Technician</th>
              <th>Reason</th>
              <th>Status</th>
            </tr>
          </thead>
          <tbody>
            {appointments.map((appointment) => (
              <tr key={appointment.id}>
                <td>{appointment.vin}</td>
                <td>{vins.includes(appointment.vin) ? "Yes" : "No"}</td>
                <td>{appointment.customer}</td>
                <td>{new Date(appointment.date_time).toLocaleDateString()}</td>
                <td>{new Date(appointment.date_time).toLocaleTimeString()}</td>
                <td>{appointment.technician}</td>
                <td>{appointment.reason}</td>
                <td>{appointment.status}</td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
}
