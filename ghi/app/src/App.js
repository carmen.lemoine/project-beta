import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainPage from "./MainPage";
import Nav from "./Nav";
import ListManufacturers from "./Manufacturer.js";
import ListModels from "./Model.js";
import ListAutomobiles from "./Automobile.js";
import { useState, useEffect } from "react";
import ManufacturerForm from "./ManufacturerForm";
import ModelForm from "./ModelForm.js";
import AutomobileForm from "./AutomobileForm";
import ListTechnicians from "./Technician.js";
import TechnicianForm from "./TechnicianForm.js";
import ListAppointments from "./Appointment.js";
import AppointmentForm from "./AppointmentForm.js";
import AppointmentHistory from "./AppointmentHistory.js";
import SalespersonForm from "./SalespersonForm.js";
import ListSalespeople from "./Salespeople";
import ListCustomers from "./Customers";
import AddCustomer from "./CustomerForm";
import ListSales from "./Sales";
import SaleForm from "./SaleForm";
import SalespersonHistory from "./SalespersonHistory";

function App() {
  const [manufacturers, setManufacturers] = useState([]);
  const [models, setModels] = useState([]);
  const [automobiles, setAutomobiles] = useState([]);
  const [technicians, setTechnicians] = useState([]);
  const [appointments, setAppointments] = useState([]);
  const [salespeople, setSalespeople] = useState([]);
  const [customers, setCustomers] = useState([]);
  const [sales, setSales] = useState([]);

  async function loadManufacturers() {
    const response = await fetch("http://localhost:8100/api/manufacturers/");
    if (response.ok) {
      const data = await response.json();
      setManufacturers(data.manufacturers);
    } else {
      console.error(response);
    }
  }

  async function loadModels() {
    const response = await fetch("http://localhost:8100/api/models/");
    if (response.ok) {
      const data = await response.json();
      setModels(data.models);
    } else {
      console.error(response);
    }
  }

  async function loadAutomobiles() {
    const response = await fetch("http://localhost:8100/api/automobiles/");
    if (response.ok) {
      const data = await response.json();
      setAutomobiles(data.autos);
    } else {
      console.error(response);
    }
  }

  async function loadTechnicians() {
    const response = await fetch("http://localhost:8080/api/technicians/");
    if (response.ok) {
      const data = await response.json();
      setTechnicians(data.technicians);
    } else {
      console.error(response);
    }
  }

  async function loadAppointments(filter = "") {
    const response = await fetch("http://localhost:8080/api/appointments/");
    if (response.ok) {
      const data = await response.json();
      if (filter === "") {
        setAppointments(data.appointments);
      } else {
        setAppointments(
          data.appointments.filter(
            (appointment) => appointment["vin"] === filter
          )
        );
      }
    } else {
      console.error(response);
    }
  }

  async function loadSalespeople() {
    const response = await fetch("http://localhost:8090/api/salespeople/");
    if (response.ok) {
      const data = await response.json();
      setSalespeople(data.salespeople);
    } else {
      console.error(response);
    }
  }

  async function loadCustomers() {
    const response = await fetch("http://localhost:8090/api/customers/");
    if (response.ok) {
      const data = await response.json();
      setCustomers(data.customers);
    } else {
      console.error(response);
    }
  }

  async function loadSales() {
    const response = await fetch("http://localhost:8090/api/sales/");
    if (response.ok) {
      const data = await response.json();
      setSales(data.sales);
    } else {
      console.error(response);
    }
  }

  useEffect(() => {
    loadManufacturers();
    loadModels();
    loadAutomobiles();
    loadTechnicians();
    loadAppointments();
    loadSalespeople();
    loadCustomers();
    loadSales();
  }, []);

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="manufacturers">
            <Route
              index
              element={<ListManufacturers manufacturers={manufacturers} />}
            />
            <Route
              path="new"
              element={
                <ManufacturerForm loadManufacturers={loadManufacturers} />
              }
            />
          </Route>
          <Route path="models">
            <Route index element={<ListModels models={models} />} />
            <Route
              path="new"
              element={
                <ModelForm
                  manufacturers={manufacturers}
                  loadModels={loadModels}
                />
              }
            />
          </Route>
          <Route path="automobiles">
            <Route index element={<ListAutomobiles autos={automobiles} />} />
            <Route
              path="new"
              element={
                <AutomobileForm
                  models={models}
                  loadAutomobiles={loadAutomobiles}
                />
              }
            />
          </Route>
          <Route path="technicians">
            <Route
              index
              element={<ListTechnicians technicians={technicians} />}
            />
            <Route
              path="new"
              element={<TechnicianForm loadTechnicians={loadTechnicians} />}
            />
          </Route>
          <Route path="appointments">
            <Route
              index
              element={
                <ListAppointments
                  appointments={appointments}
                  autos={automobiles}
                  loadAppointments={loadAppointments}
                />
              }
            />
            <Route
              path="history"
              element={
                <AppointmentHistory
                  appointments={appointments}
                  autos={automobiles}
                  loadAppointments={loadAppointments}
                />
              }
            />
            <Route
              path="new"
              element={
                <AppointmentForm
                  appointments={appointments}
                  technicians={technicians}
                  loadAppointments={loadAppointments}
                  customers={customers}
                />
              }
            />
          </Route>
          <Route path="salespeople">
            <Route
              index
              element={<ListSalespeople salespeople={salespeople} />}
            />
            <Route
              path="new"
              element={<SalespersonForm loadSalespeople={loadSalespeople} />}
            />
          </Route>
          <Route path="customers">
            <Route index element={<ListCustomers customers={customers} />} />
            <Route
              path="new"
              element={<AddCustomer loadCustomers={loadCustomers} />}
            />
          </Route>
          <Route path="sales">
            <Route index element={<ListSales sales={sales} />} />
            <Route
              path="new"
              element={
                <SaleForm
                  autos={automobiles}
                  salespeople={salespeople}
                  customers={customers}
                  loadAutomobiles={loadAutomobiles}
                  loadSales={loadSales}
                />
              }
            />
            <Route
              path="history"
              element={
                <SalespersonHistory
                  salespeople={salespeople}
                  sales={sales}
                  loadSales={loadSales}
                />
              }
            />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
