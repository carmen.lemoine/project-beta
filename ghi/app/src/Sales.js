import React from "react";

export default function ListSales({ sales }) {
  return (
    <div>
      <div className="row my-3">
        <h1> Sales </h1>
      </div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Employee ID</th>
            <th>Salesperson</th>
            <th>Customer</th>
            <th>VIN</th>
            <th>Price</th>
          </tr>
        </thead>
        <tbody>
          {sales.map((sale) => {
            return (
              <tr key={sale.id}>
                <td>{sale.salesperson.employee_id}</td>
                <td>
                  {sale.salesperson.last_name}, {sale.salesperson.first_name}
                </td>
                <td>
                  {sale.customer.first_name} {sale.customer.last_name}
                </td>
                <td>{sale.automobile.vin}</td>
                <td>${sale.price}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}
